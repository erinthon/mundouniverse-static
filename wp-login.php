<!DOCTYPE html>
	<!--[if IE 8]>
		<html xmlns="http://www.w3.org/1999/xhtml" class="ie8" lang="pt-BR">
	<![endif]-->
	<!--[if !(IE 8) ]><!-->
		<html xmlns="http://www.w3.org/1999/xhtml" lang="pt-BR">
	<!--<![endif]-->
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Mundo Universe &lsaquo; Fazer login</title>
	<link rel='dns-prefetch' href='//s.w.org' />
<script type='text/javascript'>
/* <![CDATA[ */
var LWA = {"ajaxurl":"http:\/\/mundouniverse-publisher.herokuapp.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='/wp-admin/load-scripts.php?c=1&amp;load%5B%5D=jquery-core,jquery-migrate&amp;ver=4.7.4'></script>
<script type='text/javascript' src='/wp-content/themes/15zine/plugins/login-with-ajax/login-with-ajax.js?ver=3.1.7'></script>
<link rel='stylesheet' href='/wp-admin/load-styles.php?c=1&amp;dir=ltr&amp;load%5B%5D=dashicons,buttons,forms,l10n,login&amp;ver=4.7.4' type='text/css' media='all' />
<link rel='stylesheet' id='login-with-ajax-css'  href='/wp-content/themes/15zine/plugins/login-with-ajax/widget.css?ver=3.1.7' type='text/css' media='all' />
<meta name='robots' content='noindex,follow' />
	<meta name="viewport" content="width=device-width" />
		</head>
	<body class="login login-action-login wp-core-ui  locale-pt-br">
		<div id="login">
		<h1><a href="https://br.wordpress.org/" title="Powered by WordPress" tabindex="-1">Mundo Universe</a></h1>
	
<form name="loginform" id="loginform" action="/wp-login.php" method="post">
	<p>
		<label for="user_login">Nome de usuário ou endereço de e-mail<br />
		<input type="text" name="log" id="user_login" class="input" value="" size="20" /></label>
	</p>
	<p>
		<label for="user_pass">Senha<br />
		<input type="password" name="pwd" id="user_pass" class="input" value="" size="20" /></label>
	</p>
		<p class="forgetmenot"><label for="rememberme"><input name="rememberme" type="checkbox" id="rememberme" value="forever"  /> Lembrar-me</label></p>
	<p class="submit">
		<input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="Fazer login" />
		<input type="hidden" name="redirect_to" value="http://mundouniverse-publisher.herokuapp.com/wp-admin/" />
		<input type="hidden" name="testcookie" value="1" />
	</p>
</form>

<p id="nav">
	<a href="/wp-login.php?action=lostpassword">Perdeu a senha?</a>
</p>

<script type="text/javascript">
function wp_attempt_focus(){
setTimeout( function(){ try{
d = document.getElementById('user_login');
d.focus();
d.select();
} catch(e){}
}, 200);
}

wp_attempt_focus();
if(typeof wpOnload=='function')wpOnload();
</script>

	<p id="backtoblog"><a href="/">&larr; Voltar para Mundo Universe</a></p>
	
	</div>

	
	<link rel='stylesheet' id='jetpack_css-css'  href='/wp-content/plugins/jetpack/css/jetpack.css?ver=4.9' type='text/css' media='all' />
	<div class="clear"></div>
	</body>
	</html>
	